import os,sys

import numpy as np

import math
import tqdm

from pylmgc90 import chipy

import pickle
f=open('./sample.p','rb')
R_cir=pickle.load(f)
n_branch=pickle.load(f)
r_p=pickle.load(f)
rhog_num=pickle.load(f)
Rext=pickle.load(f)
Omegaz=pickle.load(f)
f.close()

#====================================================================
#
dim =2
#
# info gestion du temps
dt       = 1.e-3
theta    = 0.5
nb_steps = 2000

echo = 0         # bavardage de certaines fonctions

freq_write   = 100
freq_display = 100

ref_radius   = r_p

# info contact

#       123456789012345678901234567890
stored = 'Stored_Delassus_Loops  '
norm   = 'Quad '
tol    = 0.1666e-3
relax  = 1.0
gs_it1 = 51
gs_it2 = 1001
#
restart_outbox = 0
#
#====================================================================

chipy.Initialize()
chipy.utilities_DisableLogMes()

chipy.checkDirectories()

####

chipy.SetDimension(dim)
#
chipy.utilities_logMes('INIT TIME STEPPING')
chipy.TimeEvolution_SetTimeStep(dt)
chipy.Integrator_InitTheta(theta)
#
chipy.utilities_logMes('READ BODIES')
chipy.ReadBodies()

chipy.utilities_logMes('READ BEHAVIOURS')
chipy.ReadBehaviours()
chipy.LoadBehaviours()

chipy.utilities_logMes('READ INI DOF')
chipy.ReadIniDof(restart_outbox)

chipy.LoadTactors()

chipy.utilities_logMes('READ INI Vloc Rloc')
chipy.ReadIniVlocRloc(restart_outbox)

chipy.utilities_logMes('READ DRIVEN DOF')
chipy.RBDY2_ReadDrivenDof()
#
chipy.utilities_logMes('WRITE BODIES')
chipy.WriteBodies()

chipy.utilities_logMes('WRITE BEHAVIOURS')
chipy.WriteBehaviours()

chipy.utilities_logMes('WRITE DRIVEN DOF')
chipy.WriteDrivenDof()

### post2D ##
chipy.OpenDisplayFiles()
chipy.OpenPostproFiles()

chipy.utilities_logMes('COMPUTE MASS')
chipy.ComputeMass()



for k in tqdm.trange(1, nb_steps + 1):
   #
   chipy.utilities_logMes('itere : '+str(k))
   #
   chipy.IncrementStep()
   chipy.ComputeFext()

   chipy.ComputeBulk()
   chipy.ComputeFreeVelocity()
   #
   chipy.SelectProxTactors()
   chipy.RecupRloc()
   #
   chipy.ExSolver(stored, norm, tol, relax, gs_it1, gs_it2)
   chipy.StockRloc()
   #
   chipy.ComputeDof()
   chipy.UpdateStep()
   #
   chipy.WriteOutDof(freq_write)
   chipy.WriteOutVlocRloc(freq_write)
   #
   chipy.WriteDisplayFiles(freq_display,ref_radius)
   chipy.WritePostproFiles()



chipy.CloseDisplayFiles()
chipy.ClosePostproFiles()
chipy.Finalize()




