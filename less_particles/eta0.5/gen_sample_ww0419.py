from __future__ import print_function
import os,sys

import numpy
import math


from pylmgc90 import chipy
from pylmgc90.pre import *

if not os.path.isdir('./DATBOX'):
  os.mkdir('./DATBOX')

if '--norand' in sys.argv:
  seed = 1
else:
  seed = None
  
  
#=====================================================================

#Parametres
# Geometrie
R_cir = 0.02      # m          # Rayon circonscrit des particules
n_branch= 4      # sans unite # Nombre de branch des particules (n>2)
r_p = 0.01       # m          # rayon de petite cercle
e = r_p/R_cir

# Masse experimental
M_exp = 0.1      # kg
# Rayon de tambour
Rext = 0.3        # m
# Vitesse angulaire du tambour
Omegaz = 6*2*math.pi/60 # rad/s

# coefficient de frottement entre les paricules
mu1 = 0.2         #
# coefficient de frottement avec les parois
mu2 = 0.9         #

nb_particles=4000


#=====================================================================

grains =[]


#fonction de calcul la densite equivalant
def calculeRhog(M_exp,R_cir,r_p,n_branch):
    S_num = 0         # S_num est la somme de surface des composants
    rhog_num = 0      # rhog_num est la densite equivalant pour la simulation
    n = n_branch
    
    # si R_cir-r_p <= apotheme du polygone, on compte la somme de surface de cercles
    if R_cir-r_p <= r_p/math.tan(math.pi/n) and r_p < R_cir:
        S_num = n*math.pi*r_p**2
        rhog_num = M_exp / S_num
        
    # si R_cir-r_p > apotheme du polygone, on compte la somme de surface de cercles, la polygone au centre et n rectangles
    elif R_cir-r_p > r_p/math.tan(math.pi/n) and r_p > 0:
        l_branch = R_cir - r_p*(1 + 1/math.tan(math.pi/n))
        S_num = n*math.pi*r_p**2 + n*r_p**2/math.tan(math.pi/n) + n*l_branch*(2*r_p) 
        rhog_num = M_exp / S_num 
    
    return rhog_num,S_num

#fonction de la creations des particules
def rigidNconvex(R_cir,e,n_branch , center, model, material, color):
 n = n_branch
 r_p = e*R_cir
 import random
 bd = avatar(dimension=2)
 bd.addNode(node(coor=numpy.array(center),number=1) )
 bd.addBulk(rigid2d())
 bd.defineGroups()
 bd.defineModel(model=model)
 bd.defineMaterial(material=material)
 
 n = n_branch
 for i in range(n):   
     bd.addContactors(shape='DISKx', color=color, byrd=r_p,
                      shift=[(R_cir-r_p)*math.cos(math.pi/n*(2*i+1)),(R_cir-r_p)*math.sin(math.pi/n*(2*i+1))])
    
 if R_cir-r_p <= r_p/math.tan(math.pi/n) and r_p < R_cir:
     pass
     
 elif R_cir-r_p > r_p/math.tan(math.pi/n) and r_p > 0:
     # Creation de la polygone au centre
     rr = r_p/math.cos(math.pi/n_branch)     # rayon circonscrit de la polygone au centre
     sommets_polygone =[]
     for i in range(n):
         sommets_polygone.append([rr*math.cos(2*math.pi/n*i),rr*math.sin(2*math.pi/n*i)])
     bd.addContactors(shape='POLYG',nb_vertices=n_branch,vertices=sommets_polygone,color='BLEUx') 
     # Creation des rectangles comme les branches
     l_branch = R_cir - r_p*(1 + 1/math.tan(math.pi/n))
     for i in range(n):
        i1 = i
        i2 = i+1
        if i == n-1:
            i2 = 0
        allonge = [l_branch*math.cos(math.pi/n*(2*i+1)),l_branch*math.sin(math.pi/n*(2*i+1))]
        sommets_rectangle = [sommets_polygone[i2],sommets_polygone[i1]]
        p3=np.sum([sommets_polygone[i1],allonge],axis = 0)
        p4=np.sum([sommets_polygone[i2],allonge],axis = 0)
        sommets_rectangle.append(p3.tolist())
        sommets_rectangle.append(p4.tolist())
        bd.addContactors(shape='POLYG',nb_vertices=4,vertices=sommets_rectangle,color='BLEUx')
        

 bd.computeRigidProperties()
 bd.rotate(description='axis', alpha=math.pi*random.random(), axis=[0., 0., 1.], center=bd.nodes[1].coor)

 return bd




# on se place en 2D
dim = 2

# creeation des conteneurs
#   * pour les corps
bodies = avatars()
#   * pour les materiaux
mat = materials()
#   * pour les tables de visibilite
svs = see_tables()
#   * pour les lois de contact
tacts = tact_behavs()

post  = postpro_commands()

# creations de deux materiaux
tdur = material(name='TDURx',materialType='RIGID',density=1000.)
plex = material(name='PLEXx',materialType='RIGID',density=100.)

rhog_num,S_num = calculeRhog(M_exp=M_exp,R_cir=R_cir,r_p=r_p,n_branch=n_branch)

mgrains = material(name='conca',materialType='RIGID',
                   density=rhog_num)
mat.addMaterial(tdur,plex,mgrains)

# on cree un modele de rigide
mod = model(name='rigid', physics='MECAx', element='Rxx2D', dimension=dim)


# distribtion aleatoire dans [0.8, 1.2[ 
radii=granulo_Random(nb_particles, 0.8*R_cir, 1.2*R_cir, seed)

# on recupere le plus petit et le plus grand rayon
radius_min=min(radii)
radius_max=max(radii)


# depot
[nb_remaining_particles, coor]=depositInBox2D(radii= radii, lx=2*Rext,ly=2*Rext)

# ajout du tambour
drum = rigidDisk(r=Rext, center=[Rext,Rext], model=mod, material=plex, color='BLEUx', is_Hollow=True)

# on ajoute le tambour a la liste des corps
bodies += drum

# si toutes les particules deposees n'ont pas ete conservees
if (nb_remaining_particles < nb_particles):
   # on affiche un avertissement
   print("Warning: granulometry changed, since some particles were removed!")

# boucle d'ajout des disques :
for i in range(0,nb_remaining_particles,1):
   # creation un nouveau disque rigide, constitue du materiau plex
   body=rigidNconvex(R_cir=radii[i],e=e,n_branch=n_branch, center=coor[2*i : 2*(i + 1)], 
                  model=mod, material=mgrains, color='BLEUx') 
   #fixer bug pour fonciton depositInDrum
   center = coor[2*i : 2*(i + 1)]
   if np.sqrt((center[0]-Rext)**2+(center[1]-Rext)**2)< Rext-radii[i]:
       grains.append(body)
       # ajout du disque dans le conteneur de corps
       bodies += body




#try:
#  visuAvatars(bodies)
#except:
#  pass

# conditions aux lmites :
#   * le tambour ne se translate pas, mais tourne a une vitese 
#     constante : 1 tour/min
drum.imposeDrivenDof(component=[1, 2, 3], dofty='vlocy')
# drum.imposeDrivenDof(component=3,dofty='vlocy',ct=math.pi/30.,rampi=1.)

# gestion des interactions :
#   * declaration des lois
#       - entre particules
ldkdk=tact_behav(name='iqsc0',law='IQS_CLB',fric=mu1)
tacts+=ldkdk
#       - avec les parois
ldkkd=tact_behav(name='iqsc1',law='IQS_CLB',fric=mu2)
tacts+=ldkkd
#   * declaration des tables de visibilite
#       - entre particules
#         1. entre disk et disk
#         2. entre disk et polygone
svdkdk = see_table(CorpsCandidat='RBDY2',candidat='DISKx',
   colorCandidat='BLEUx',behav=ldkdk, CorpsAntagoniste='RBDY2', 
   antagoniste='DISKx',colorAntagoniste='BLEUx',alert=0.1*radius_min)
svs+=svdkdk
svdkpl = see_table(CorpsCandidat='RBDY2',candidat='DISKx',
                    colorCandidat='BLEUx',behav=ldkdk, CorpsAntagoniste='RBDY2', 
                    antagoniste='POLYG',colorAntagoniste='BLEUx',alert=0.1*radius_min)
svs+=svdkpl
#       - avec le tambour :
svdkkd = see_table(CorpsCandidat='RBDY2',candidat='DISKx',
   colorCandidat='BLEUx',behav=ldkkd, CorpsAntagoniste='RBDY2', 
   antagoniste='xKSID',colorAntagoniste='BLEUx',alert=0.1*radius_min)
svs+=svdkkd

# disp = postpro_command(name="BODY TRACKING",step=1,rigid_set=grains )
# torq = postpro_command(name="TORQUE EVOLUTION",step=1,rigid_set=grains )
# sitr = postpro_command(name="SOLVER INFORMATIONS",step=1)
viol = postpro_command(name="VIOLATION EVOLUTION",step=1)

# post.addCommand(disp)
# post.addCommand(torq)
# post.addCommand(sitr)
post.addCommand(viol)

# ecriture des fichiers
writeBodies(bodies,chemin='DATBOX/')
writeBulkBehav(mat,chemin='DATBOX/',dim=dim,gravy=[0.,-9.81,0.])
writeTactBehav(tacts,svs,chemin='DATBOX/')
writeDrvDof(bodies,chemin='DATBOX/')
writeDofIni(bodies,chemin='DATBOX/')
writeVlocRlocIni(chemin='DATBOX/')

writePostpro(post, bodies, path='DATBOX/')

import pickle
f=open('sample.p','wb')
pickle.dump(R_cir,f)
pickle.dump(n_branch,f)
pickle.dump(r_p,f)
pickle.dump(rhog_num,f)
pickle.dump(Rext,f)
pickle.dump(Omegaz,f)
f.close()




