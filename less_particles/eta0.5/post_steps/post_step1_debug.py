import tqdm
import time
import numpy as np
from numpy import linalg as LA

import pandas as pd
from scipy import stats

import matplotlib.pyplot as plt
import seaborn as sns

import os
import sys

#path_my_post=os.path.join('..','..','..','src')
path_my_post='../../../src' 
sys.path.append(path_my_post)
import mypost_voronoi as mpv

os.makedirs('pickles', exist_ok=True)  

import pickle
# read parametres when generate the sample
f=open('../sample.p','rb')
R_cir=pickle.load(f)
n_branch=pickle.load(f)
r_p=pickle.load(f)
rhog_num=pickle.load(f)
Rext=pickle.load(f)
Omegaz=pickle.load(f)
f.close()


# =============================================================================
g=9.81                  #gravity
lgrid=3*R_cir           #length of grid
N=2*int(Rext/lgrid)     #size of grid N*N
bins=np.arange(-Rext,Rext,lgrid)

#bande sigma
demi_bande=9*R_cir
n=demi_bande/lgrid
i_bande=np.arange(0.5*N+1-n,0.5*N+1+n)

#Unit
stress_unit = rhog_num*g*(2*R_cir)
velocity_unit = np.sqrt(2*R_cir*g)
# =============================================================================

import fnmatch
nb_files_remake= len(fnmatch.filter(os.listdir('../pickles_remake_Df'),'*.pkl'))
nb_files_step1= len(fnmatch.filter(os.listdir('./pickles'),'post_step1.pkl*'))
print(nb_files_step1,'/',nb_files_remake)

def readPkl(k):
    name='post_pickle_'+str(k)
    path='../pickles_remake_Df/'+name+'.pkl'
    evol_dict=pd.read_pickle(path) 
    return evol_dict


def findGrid(cd_array):
    i2p = np.digitize(np.array(cd_array[:,0]),bins)
    j2p = np.digitize(np.array(cd_array[:,1]),bins)
    igrid2p = N*i2p+j2p
    ij=np.vstack([igrid2p,i2p,j2p]).transpose()
    return ij        
    
def sortTypeConnectOnlyDKDK(ser):
    df=inters_df.iloc[ser.index]
    nb_connect=ser.sum()
    if nb_connect==1:
        return 'type1-1_p'
    elif nb_connect==2:
        if df['icdtac'].values[0]!=df['icdtac'].values[1] and df['iantac'].values[0]!=df['iantac'].values[1]:
            return 'type2-2_p'
        else:
            return 'type2-1_p'
    elif nb_connect==3:
        return 'type3_p'
    else:
        print(ser)
        
def makeTensorContrainteDf(ir,sign):
    cd = rbdy2_df.at[ir,'cd']
    cd_inter = row_inter.at['cd_inter']
    r=np.array([cd_inter[0]-cd[0],cd_inter[1]-cd[1]])
    f=sign*np.array([row_inter.at['Rx'],row_inter.at['Ry']])
    s_tensor=np.outer(f,r) 
    contrainte_dict={'number': ir,
                    's11': s_tensor[0,0],'s12': s_tensor[0,1],
                    's21': s_tensor[1,0],'s22': s_tensor[1,1]}
    return contrainte_dict


def makeTensorFabricDf(row):
    cd1 = rbdy2_df.at[row['particle1'],'cd']
    cd2 = rbdy2_df.at[row['particle1'],'cd']
    cd_inter = row.at['cd_inter']
    na=np.array([cd_inter[0]-cd1[0],cd_inter[1]-cd1[1]])
    na=na/LA.norm(na)
    nb=np.array([cd_inter[0]-cd2[0],cd_inter[1]-cd2[1]])
    nb=nb/LA.norm(nb)
    fabric=np.outer(na,nb) 
    return np.ravel(fabric)

def storeTensorFabricDf(ir):
    fabric_dict={'number': ir,
                'F11': row_contact['F11'],'F12': row_contact['F12'],
                'F21': row_contact['F21'],'F22': row_contact['F22']}
    return fabric_dict

def changeFrameTensor(row):
    tensor = row.to_numpy().reshape(2,2)
    MM = np.matrix(M)
    tensor_xz = MM.I*tensor*MM
    tensor_xz = pd.Series(np.ravel(tensor_xz))
    return tensor_xz

def calculeEgivals(row):
    if row.isnull().any():
        v=[np.nan,np.nan]
    else:
        tensor = row.to_numpy().reshape(2,2)
        v=LA.eigvals(tensor)
    return pd.Series(v)
    
    
print('\ndealing datas')
# for k in tqdm.trange(20+nb_files_step1, 20+nb_files_remake):
for k in tqdm.trange(22, 23):
    row_evol=readPkl(k)
    rbdy2_df=row_evol['rbdy2_df'].set_index(['number'])
    inters_df=row_evol['inters_df']
    DISKx_df=row_evol['DISKx_df']
    POLYG_df=row_evol['POLYG_df']
    #Note the grid belong for each rbdy2
    cd_array=np.vstack(rbdy2_df.cd)
    rbdy2_df[['igrid','i','j']]=findGrid(cd_array)    
    
    
# ======================= Free Surface & Slope angle ===================================
    layer_df=rbdy2_df[['cd','i','j']].copy()
    layer_df['x']=cd_array[:,0]
    layer_df['y']=cd_array[:,1]
    layer_df=layer_df.loc[layer_df.groupby('i')['y'].idxmax()]
    layer_x=np.array(layer_df.x)
    layer_y=np.array(layer_df.y+R_cir)
    
    #pop some point near the drum for line regression
    line_x = []; line_y = []
    for i in range(len(layer_x)):
        if layer_x[i]>0 and mpv.halfDrumRight(layer_y[i])-layer_x[i]<lgrid:
            break
        line_x.append(layer_x[i])
        line_y.append(layer_y[i])
        
    #slope, intercept, r_value, p_value, stderr
    linereg = stats.linregress(line_x,line_y)
    theta = np.arctan(linereg.slope)

    #matrix for changing the frame
    s=np.sin(theta);c=np.cos(theta)
    M = [-c,-s,-s,c]
    M = np.array(M).reshape(2,2)
    
# ======================== Volume Local & Stress Tensor =============================
    #mask for volume voronoi
    maskx=[-Rext,Rext,mpv.halfDrumLeft(layer_y[0]),mpv.halfDrumRight(layer_y[-1])]
    masky=[-Rext,-Rext,layer_y[0],layer_y[-1]]
    maskx.extend(layer_x)
    masky.extend(layer_y)
    mask_points=np.stack((maskx,masky), axis=1)
    #add volume voronoi and volume fraction to rbdy2_df
    df=mpv.VoronoiDiagram(DISKx_df,mask_points)
    rbdy2_df = pd.concat([rbdy2_df, df], axis=1)
    
    #For the Stress Tensor
    contrainte_df = pd.DataFrame(columns = ['number']) 
    for index,row_inter in inters_df.iterrows():
        ir1=row_inter['particle1']
        ir2=row_inter['particle2']
        contrainte_df = contrainte_df.append(makeTensorContrainteDf(ir1,-1), ignore_index=True)
        contrainte_df = contrainte_df.append(makeTensorContrainteDf(ir2,+1), ignore_index=True)
    contrainte_df=contrainte_df.groupby('number').agg('sum')
    rbdy2_df=pd.concat([rbdy2_df, contrainte_df], axis=1)
    
    #adimension stress and velocity
    rbdy2_df[['s11', 's12', 's21','s22']] = rbdy2_df[['s11', 's12', 's21','s22']].div(rbdy2_df.volume_voronoi*stress_unit, axis=0)
    rbdy2_df['v_xy'] = rbdy2_df['v_xy'].map(lambda x: np.array(x))
    rbdy2_df['v_xy'] = rbdy2_df['v_xy'].div(velocity_unit, axis=0)
    
    
# ==================== Contact Type & Fabric Tensor & Coordinate Number =============================

    #sort the contact of type by connectivity 
    inters_df['type_connect']=inters_df.groupby(['particle1','particle2'])['nb_connect'].transform(sortTypeConnectOnlyDKDK)  
    
    groups=inters_df.groupby(['particle1','particle2'])
    
    inters_df[['F11', 'F12', 'F21','F22']]=inters_df.apply(makeTensorFabricDf, axis=1, result_type='expand')
    
    #For the coordinate number
    connect_df1 = groups.agg({'nb_connect':'count'})
    connect_df2 = connect_df1.reorder_levels([1, 0], axis=0)
    connect_df2 = connect_df2.sort_index()
    connect_df2.index=connect_df2.index.set_names('particle1', level=0)
    connect_df2.index=connect_df2.index.set_names('particle2', level=1)
    connect_df = pd.concat([connect_df1, connect_df2])
    connect_df = connect_df.sort_index()
    
    z_df = connect_df.groupby(level='particle1').count().rename(columns={'nb_connect': 'nb_voisin'})
    zc_df = connect_df.groupby(level='particle1').sum().rename(columns={'nb_connect': 'nb_connect'})
    z_df.index=z_df.index.set_names('number')
    zc_df.index=zc_df.index.set_names('number')
    
    rbdy2_df = pd.concat([rbdy2_df, z_df], axis=1)
    rbdy2_df = pd.concat([rbdy2_df, zc_df], axis=1)

    # sum the connectivity for each contact
    contact_df = groups.agg({'nb_connect':'count','Rx':np.sum,'Ry':np.sum,'type_connect':np.min,
                              'F11':np.sum, 'F12':np.sum, 'F21':np.sum,'F22':np.sum})
    
    contact_df[['F11', 'F12', 'F21','F22']] = contact_df[['F11', 'F12', 'F21','F22']].div(contact_df.nb_connect, axis=0)
    
    # store tensor fabric for each rbdy2
    fabric_df=pd.DataFrame(columns = ['number']) 
    for index,row_contact in contact_df.iterrows():
        ir1=row_contact.name[0]
        ir2=row_contact.name[1]
        fabric_df = fabric_df.append(storeTensorFabricDf(ir1), ignore_index=True)
        fabric_df = fabric_df.append(storeTensorFabricDf(ir2), ignore_index=True)
    fabric_df=fabric_df.groupby('number').agg(np.sum)
    rbdy2_df=pd.concat([rbdy2_df, fabric_df], axis=1)
    
    rbdy2_df[['F_i','F_ii']]=rbdy2_df[['F11', 'F12', 'F21','F22']].apply(calculeEgivals,axis=1)
    
# ====================In the frame exez =================================
    
    rbdy2_exz=rbdy2_df[['r_p','w','nb_voisin','nb_connect','volume_fraction']].copy()
    rbdy2_exz['cd'] = rbdy2_df['cd'].map(lambda x: np.dot(M,x))
    rbdy2_exz['v_ex'] = rbdy2_df['v_xy'].map(lambda x: np.array(np.dot(M,x))[0])
    rbdy2_exz['v_ez'] = rbdy2_df['v_xy'].map(lambda x: np.array(np.dot(M,x))[1])

    rbdy2_exz[['igrid','i','j']]=findGrid(np.vstack(rbdy2_exz.cd))    
    rbdy2_exz=rbdy2_exz.rename(columns={'cd': 'cd_exz'})
    
    rbdy2_exz[['s11', 's12', 's21','s22']] = rbdy2_df[['s11', 's12', 's21','s22']].apply(changeFrameTensor,axis=1)
    rbdy2_exz[['F11', 'F12', 'F21','F22']] = rbdy2_df[['F11', 'F12', 'F21','F22']].apply(changeFrameTensor,axis=1)
    rbdy2_exz[['F_i','F_ii']]=rbdy2_exz[['F11', 'F12', 'F21','F22']].apply(calculeEgivals,axis=1)
    


# =============================================================================  

    post_step1_dict={'layer': [layer_x,layer_y],
                     'linereg': linereg,
                     'angle':theta,
                     }
    
    post_step1_df={'Time': row_evol['Time'],
                       'rbdy2_df': rbdy2_df,
                       'rbdy2_exz':rbdy2_exz,
                       'DISKx_df': DISKx_df,
                       'POLYG_df': POLYG_df,
                       'inters_df':inters_df,
                       'contact_df':contact_df,
                       'post_step1_dict':post_step1_dict
                       }

    
# ====================== Figure: Particles & Free Surface & Line Regression ===================================
    
    # fig= plt.figure(figsize=(12, 9))
    
    # theta_t = np.linspace(0, 2 * np.pi, 200)
    # xt=np.cos(theta_t)
    # yt=np.sin(theta_t)
    
    # ax = fig.add_subplot(111)
    # # for indexd,rowd in DISKx_df.iterrows():
    # #     cd_disk_x=rowd['position_x'];cd_disk_y=rowd['position_y'];rr=rowd['r_p']
    # #     ax.add_patch(plt.Circle((cd_disk_x,cd_disk_y),rr,alpha=0.5))
        
    # plt.plot(Rext*xt,Rext*yt,label='tambour')
    # plt.plot(layer_x,layer_y,label='free surface')
    # plt.plot(np.array(layer_x),linereg.slope*np.array(layer_x)+linereg.intercept,label='regress')
    # xs=np.array([Rext*(-s),Rext*(s)])
    # ys=np.array([Rext*(c),Rext*(-c)])
    # plt.plot(xs-n*lgrid*c,ys-n*lgrid*s,'r--',label='$\Sigma$',alpha=0.5)
    # plt.plot(xs+n*lgrid*c,ys+n*lgrid*s,'r--',alpha=0.5)
    # plt.axis('equal')
    # plt.legend()
    # plt.show()
    
# ======================== Figure: Fraction & Velocity ================================
    
#    fig= plt.figure(figsize=(12, 9))
#    ax1 = fig.add_subplot(311)
#    ax2 = fig.add_subplot(312)
#    ax3 = fig.add_subplot(313)
#
#    bande_df=rbdy2_exz[rbdy2_exz['i'].isin(i_bande)].copy()
#    bande_df['v_ex_mean']=bande_df.groupby('j')['v_ex'].transform(np.mean)
#    bande_df['Temperature_x']=bande_df.apply(lambda x: (x['v_ex']-x['v_ex_mean'])**2, axis=1)
#
#    df=bande_df.groupby(['j']).agg({'v_ex':np.mean,'volume_fraction':np.mean,
#                                    'Temperature_x':np.mean})
#
#
#    df['h (unité:d)']=df.apply(lambda x: x.name*1.5,axis=1, result_type='expand')
#    sns.lineplot(x = 'h (unité:d)', y = 'volume_fraction',
#                    data = df,ax = ax1)
#    sns.lineplot(x = 'h (unité:d)', y = 'v_ex',
#                    data = df,ax = ax2)
#    sns.lineplot(x = 'h (unité:d)', y = 'Temperature_x',
#                      data = df,ax = ax3)
#    plt.show()

# ==================== Figure: Coordinate Number ===============================================
    
    # fig= plt.figure(figsize=(12, 9))
    
    # theta_t = np.linspace(0, 2 * np.pi, 200)
    # xt=np.cos(theta_t)
    # yt=np.sin(theta_t)
    
    # xs=0.5*N*np.array([-s,s])+0.5*N
    # ys=0.5*N*np.array([c,-c])+0.5*N
    
    # df1=rbdy2_df.groupby(['j','i'])['nb_connect'].apply(np.mean).unstack()
    # df2=rbdy2_exz.groupby(['j','i'])['nb_connect'].apply(np.mean).unstack()
    # ax1 = fig.add_subplot(231)
    # ax2 = fig.add_subplot(232)
    # ax3 = fig.add_subplot(233)
    

    # df = pd.DataFrame(np.nan,index= np.arange(N+1),columns=np.arange(N+1)).drop([0]).drop([0], axis=1)
    # df.loc[df1.index,df1.columns]=df1
    
    # ax1.plot(0.5*N*(xt+1),0.5*N*(yt+1),label='tambour')
    # ax2.plot(0.5*N*(xt+1),0.5*N*(yt+1),label='tambour')
    # ax3.plot(0.5*N*(xt+1),0.5*N*(yt+1),label='tambour')
    # ax1.plot(xs-n*c,ys-n*s,'r--',label='$\Sigma$',alpha=0.5)
    # ax1.plot(xs+n*c,ys+n*s,'r--',alpha=0.5)
    # ax2.plot([0.5*N+n,0.5*N+n],[0,N],'r--',alpha=0.5)
    # ax2.plot([0.5*N-n,0.5*N-n],[0,N],'r--',alpha=0.5)
    # ax3.plot([0.5*N+n,0.5*N+n],[0,N],'r--',alpha=0.5)
    # ax3.plot([0.5*N-n,0.5*N-n],[0,N],'r--',alpha=0.5)
    
    
    # sns.heatmap(df,vmin=-0.5,vmax=6.5,cmap=sns.color_palette("coolwarm", 7,as_cmap=False),
    #             ax=ax1,cbar_kws={'ticks': np.arange(7),"shrink": .8})
    # sns.heatmap(df2,vmin=-0.5,vmax=6.5,cmap=sns.color_palette("coolwarm", 7,as_cmap=False),
    #             ax=ax2,cbar_kws={'ticks': np.arange(7),"shrink": .8})
    
    
    
    # ax1.invert_yaxis()
    # ax1.axis('equal')
    # ax1.set_title('In the Frame xy')
    # ax2.invert_yaxis()
    # ax2.invert_xaxis()
    # ax2.axis('equal')
    # ax2.set_title('In the Frame exez')
    
    # ax4 = fig.add_subplot(212)
    # bande_df=rbdy2_exz[rbdy2_exz['i'].isin(i_bande)].copy()
    
    # df3=bande_df.groupby(['j','i'])['nb_connect'].apply(np.mean).unstack()
    # df = pd.DataFrame(np.nan,index= np.arange(N+1),columns=np.arange(N+1)).drop([0]).drop([0], axis=1)
    # df.loc[df3.index,df3.columns]=df3
    
    # sns.heatmap(df,vmin=-0.5,vmax=6.5,cmap=sns.color_palette("coolwarm", 7,as_cmap=False),
    #         ax=ax3,cbar_kws={'ticks': np.arange(7),"shrink": .8})

    # ax3.invert_yaxis()
    # ax3.invert_xaxis()
    # ax3.axis('equal')
    # ax3.set_title('Bande in the Frame exez')

    # df=bande_df.groupby(['j']).agg({'nb_connect':np.mean,'nb_voisin':np.mean})
    # df['h (unité:d)']=df.apply(lambda x: x.name*lgrid/R_cir,axis=1, result_type='expand')

    # sns.lineplot(data=df.set_index('h (unité:d)'),
    #               ax=ax4)
        
    # plt.show()

# =============================================================================

    fig= plt.figure(figsize=(12, 9))
    
    theta_t = np.linspace(0, 2 * np.pi, 200)
    xt=np.cos(theta_t)
    yt=np.sin(theta_t)
    
    xs=0.5*N*np.array([-s,s])+0.5*N
    ys=0.5*N*np.array([c,-c])+0.5*N
    
    df1=rbdy2_df.groupby(['j','i'])['s22'].apply(np.mean).unstack()
    df2=rbdy2_exz.groupby(['j','i'])['s22'].apply(np.mean).unstack()
    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222)
    
    
    df = pd.DataFrame(np.nan,index= np.arange(N+1),columns=np.arange(N+1)).drop([0]).drop([0], axis=1)
    df.loc[df1.index,df1.columns]=df1
    
    ax1.plot(0.5*N*(xt+1),0.5*N*(yt+1),label='tambour')
    ax2.plot(0.5*N*(xt+1),0.5*N*(yt+1),label='tambour')
    ax1.plot(xs-n*c,ys-n*s,'r--',label='$\Sigma$',alpha=0.5)
    ax1.plot(xs+n*c,ys+n*s,'r--',alpha=0.5)
    ax2.plot([0.5*N+n,0.5*N+n],[0,N],'r--',alpha=0.5)
    ax2.plot([0.5*N-n,0.5*N-n],[0,N],'r--',alpha=0.5)
    
    
    sns.heatmap(df,ax=ax1,cmap=sns.color_palette("Blues",as_cmap=True))
    sns.heatmap(df2, ax=ax2,cmap=sns.color_palette("Blues",as_cmap=True))
    
    ax1.invert_yaxis()
    ax1.axis('equal')
    ax1.set_title('In the Frame xy')
    ax2.invert_yaxis()
    ax2.invert_xaxis()
    ax2.axis('equal')
    ax2.set_title('In the Frame exez')
     
    ax4 = fig.add_subplot(212)
    bande_df=rbdy2_exz[rbdy2_exz['i'].isin(i_bande)].copy()

    df=bande_df.groupby(['j']).agg({'s11':np.mean,'s12':np.mean,'s21':np.mean,'s22':np.mean})
    df['h (unité:d)']=df.apply(lambda x: x.name*lgrid/R_cir,axis=1, result_type='expand')

    sns.lineplot(data=df.set_index('h (unité:d)'),
                   ax=ax4)
     
#    name='post_step1.pkl'+str(k)
#    path='./pickles/'+name
#    f = open(path, "wb" )
#    pickle.dump(post_step1_df, f )
#    f.close()





