import tqdm
import numpy as np
import pandas as pd
from scipy import stats

import matplotlib.pyplot as plt
import seaborn as sns

import sys,os

path_my_post=os.path.join('..','..','..','src') 
sys.path.append(path_my_post)
import mypost_voronoi as mpv

import fnmatch
nb_files_step1= len(fnmatch.filter(os.listdir('./pickles'),'post_step1.pkl*'))

import pickle
# read parametres when generate the sample
f=open('../sample.p','rb')
R_cir=pickle.load(f)
n_branch=pickle.load(f)
r_p=pickle.load(f)
rhog_num=pickle.load(f)
Rext=pickle.load(f)
Omegaz=pickle.load(f)
f.close()


# =============================================================================
lgrid=3*R_cir           #length of grid
N=2*int(Rext/lgrid)     #size of grid N*N
bins=np.arange(-Rext,Rext,lgrid)

#bande sigma
demi_bande=3*R_cir
n=demi_bande/lgrid
i_bande=np.arange(0.5*N+1-n,0.5*N+1+n)    # the grid i in the bande

# =============================================================================

def readPklStep1(k):
    name='post_step1.pkl'+str(k)
    path='./pickles/'+name
    f=open(path,'rb')
    post_step1_df=pickle.load(f)
    f.close()
    
    return post_step1_df



void_df=pd.DataFrame()
needs = {'volume_fraction':void_df,'v_ex':void_df,'v_ez':void_df,
          'nb_voisin':void_df,'nb_connect':void_df,
          'F11':void_df, 'F12':void_df, 'F21':void_df,'F22':void_df,
          'F_i':void_df,'F_ii':void_df}


agg_methode={'volume_fraction':np.mean,'v_ex':np.mean,'v_ez':np.mean,
             'nb_voisin':np.mean,'nb_connect':np.mean,
             'F11':np.mean, 'F12':np.mean, 'F21':np.mean,'F22':np.mean,
             'F_i':np.mean,'F_ii':np.mean}


surface_df=void_df
flow_df=void_df

# for k in tqdm.trange(80, 20+nb_files_step1):
for k in tqdm.trange(80, 83):
    post_step1_df=readPklStep1(k)
    post_step1_dict=post_step1_df['post_step1_dict']
    
    rbdy2_exz=post_step1_df['rbdy2_exz']
    
# =================== Free surface & Flow =======================================

    cd_array=np.vstack(rbdy2_exz.cd_exz)

    layer_df=rbdy2_exz[['i','j']].copy()
    layer_df['z']=cd_array[:,1]+Rext
    layer_df['z1']=cd_array[:,1]+R_cir+Rext
    surface_df=pd.concat([surface_df,layer_df.groupby('i')['z1'].max()],axis=1)
    
    layer_df['abs_vex']= abs(np.vstack(rbdy2_exz.v_ex))
    flow_k=layer_df.loc[layer_df.groupby('i')['abs_vex'].idxmin()]
    flow_k=flow_k.set_index('i')
    flow_df=pd.concat([flow_df,flow_k['z']],axis=1)
    
    layer_z=np.array(layer_df.z+R_cir)

# =============================================================================

    grid_k=rbdy2_exz.groupby('igrid').agg(agg_methode)
    for key, need_df in needs.items():
        needs[key] = pd.concat([need_df,grid_k.loc[:,key]],axis=1)
        
    
grid_dict = {}
for key, need_df in needs.items():
    grid_dict.update({key:need_df.mean(axis=1)})

grid_df=pd.DataFrame(grid_dict)
print('\n',grid_df)


layer_need = pd.DataFrame({'free_surface':surface_df.mean(axis=1),'free_surface_std':surface_df.std(axis=1),
                           'flow_band':flow_df.mean(axis=1), 'flow_band_std':surface_df.std(axis=1)})

name='./step2_grid_exz.p'
f = open(name, "wb" )
pickle.dump(grid_df, f )
pickle.dump(layer_need, f )
f.close()

# =============================================================================


fig= plt.figure(figsize=(16, 9))

theta = np.arctan(post_step1_dict['linereg'].slope)
s=np.sin(theta);c=np.cos(theta)

theta_t = np.linspace(0, 2 * np.pi, 200)
xt=np.cos(theta_t)
yt=np.sin(theta_t)

xs=0.5*N*np.array([-s,s])+0.5*N
ys=0.5*N*np.array([c,-c])+0.5*N

ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)

ratio=Rext/R_cir/2
ax1.plot(ratio*(xt+1),ratio*(yt+1),label='tambour')
ax2.plot(ratio*(xt+1),ratio*(yt+1),label='tambour')


layer_need=layer_need.applymap(lambda x: x/R_cir/2)
layer_need['x (unit:d)'] = layer_need.apply(lambda x: (x.name-0.5)*lgrid/R_cir/2,axis=1, result_type='expand')

layer_need.plot('x (unit:d)', 'free_surface', yerr='free_surface_std', ax=ax1,label= 'free surface')
layer_need.plot('x (unit:d)', 'flow_band', yerr='flow_band_std', ax=ax1,label= 'flow band')

layer_need.plot('x (unit:d)', 'free_surface', yerr='free_surface_std', ax=ax2, label= 'free surface')
layer_need.plot('x (unit:d)', 'flow_band', yerr='flow_band_std', ax=ax2, label= 'flow band')

df=grid_df[['v_ex','v_ez']].reset_index()
df['i'] = (df['index'].mod(N)-0.5)*lgrid/R_cir/2
df['j'] = (df['index'].floordiv(N)-0.5)*lgrid/R_cir/2
stream = df[['j','i','v_ex','v_ez']].to_numpy()

ax1.quiver(stream[:,0], stream[:,1], stream[:,2], stream[:,3])
ax1.axis('equal')
ax1.set_ylabel('y (unit:d)')
ax1.set_title('velocity field in the frame $(e_x,e_z)$')


stream_df = df[['j','i','v_ex','v_ez']]
u=df[['j','i','v_ex']].pivot(index='i', columns='j', values='v_ex').fillna(0)
v=df[['j','i','v_ez']].pivot(index='i', columns='j', values='v_ez').fillna(0)
X=df[['j','i','v_ez']].pivot(index='i', columns='j', values='j').fillna(method='ffill').fillna(method='bfill').to_numpy()
Y=df[['j','i','v_ez']].pivot(index='i', columns='j', values='i').fillna(axis=1,method='ffill').fillna(axis=1,method='bfill').to_numpy()

plt.streamplot(X, Y, u, v, density = 0.5,color='k')

ax2.axis('equal')
ax2.set_ylabel('y (unit:d)')
ax2.set_title('velocity stream in the frame $(e_x,e_z)$')

plt.savefig('field_exz.pdf',dpi=200)













