import tqdm
import numpy as np
import pandas as pd
from scipy import stats

import matplotlib.pyplot as plt
import seaborn as sns

import sys,os

path_my_post=os.path.join('..','..','..','src') 
sys.path.append(path_my_post)
import mypost_voronoi as mpv

import fnmatch
nb_files_step1= len(fnmatch.filter(os.listdir('./pickles'),'post_step1.pkl*'))

import pickle
# read parametres when generate the sample
f=open('../sample.p','rb')
R_cir=pickle.load(f)
n_branch=pickle.load(f)
r_p=pickle.load(f)
rhog_num=pickle.load(f)
Rext=pickle.load(f)
Omegaz=pickle.load(f)
f.close()


# =============================================================================
lgrid=3*R_cir           #length of grid
N=2*int(Rext/lgrid)     #size of grid N*N
bins=np.arange(-Rext,Rext,lgrid)

#bande sigma
demi_bande=3*R_cir
n=demi_bande/lgrid
i_bande=np.arange(0.5*N+1-n,0.5*N+1+n)    # the grid i in the bande

# =============================================================================

def readPklStep1(k):
    name='post_step1.pkl'+str(k)
    path='./pickles/'+name
    f=open(path,'rb')
    post_step1_df=pickle.load(f)
    f.close()
    
    return post_step1_df



void_df=pd.DataFrame()
needs = {'volume_fraction':void_df,'v_ex':void_df,'Temperature_x':void_df,
          'nb_voisin':void_df,'nb_connect':void_df,
          'F11':void_df, 'F12':void_df, 'F21':void_df,'F22':void_df,
          'F_i':void_df,'F_ii':void_df}


agg_methode={'volume_fraction':np.mean,'v_ex':np.mean,'Temperature_x':np.mean,
             'nb_voisin':np.mean,'nb_connect':np.mean,
             'F11':np.mean, 'F12':np.mean, 'F21':np.mean,'F22':np.mean,
             'F_i':np.mean,'F_ii':np.mean}

for k in tqdm.trange(80, 20+nb_files_step1):
#for k in tqdm.trange(80, 83):
    post_step1_df=readPklStep1(k)
    post_step1_dict=post_step1_df['post_step1_dict']
    
    rbdy2_df=post_step1_df['rbdy2_df']
    rbdy2_exz=post_step1_df['rbdy2_exz']

    rbdy2_band=rbdy2_exz[rbdy2_exz['i'].isin(i_bande)].copy()

    rbdy2_band['v_ex_mean']=rbdy2_band.groupby('j')['v_ex'].transform(np.mean)
    rbdy2_band['Temperature_x']=rbdy2_band.apply(lambda x: (x.v_ex-x.v_ex_mean)**2, axis=1)
    
    bande_k=rbdy2_band.groupby('j').agg(agg_methode)
    
    for key, need_df in needs.items():
        needs[key] = pd.concat([need_df,bande_k.loc[:,key]],axis=1)
        
    
bande_dict = {}
for key, need_df in needs.items():
    bande_dict.update({key:need_df.mean(axis=1)})
    key_std= str(key)+'_std'
    bande_dict.update({key_std:need_df.std(axis=1)})

bande_df=pd.DataFrame(bande_dict)
print('\n',bande_df)

name='./step2_bande_'+str(int(demi_bande/R_cir))+'.p'
f = open(name, "wb" )
pickle.dump(bande_df, f )
f.close()


# =============================================================================

#ax = fig.add_subplot(111)
#for indexd,rowd in DISKx_df.iterrows():
#    cd_disk_x=rowd['position_x'];cd_disk_y=rowd['position_y'];rr=rowd['r_p']
#    ax.add_patch(plt.Circle((cd_disk_x,cd_disk_y),rr,alpha=0.5))
#plt.plot(Rext*xt,Rext*yt,label='tambour')
#plt.plot(layer_x,layer_y)
#plt.plot(np.array(layer_x),linereg.slope*np.array(layer_x)+linereg.intercept)
#
#xs=np.array([Rext*(-s),Rext*(s)])
#ys=np.array([Rext*(c),Rext*(-c)])
#plt.plot(xs-lgrid*c,ys-lgrid*s,'r--',label='$\Sigma$',alpha=0.5)
#plt.plot(xs+lgrid*c,ys+lgrid*s,'r--',alpha=0.5)
#plt.axis('equal')
#plt.legend()

# =============================================================================
fig= plt.figure(figsize=(12, 9))

theta = np.arctan(post_step1_dict['linereg'].slope)
s=np.sin(theta);c=np.cos(theta)

theta_t = np.linspace(0, 2 * np.pi, 200)
xt=np.cos(theta_t)
yt=np.sin(theta_t)

xs=0.5*N*np.array([-s,s])+0.5*N
ys=0.5*N*np.array([c,-c])+0.5*N

ax1 = fig.add_subplot(221)
ax2 = fig.add_subplot(222)
ax3 = fig.add_subplot(212)

for ax in [ax1,ax2]:
    ax.plot(0.5*N*(xt+1),0.5*N*(yt+1),label='tambour')
    ax.plot(xs-n*c,ys-n*s,'r--',label='$\Sigma$',alpha=0.5)
    ax.plot(xs+n*c,ys+n*s,'r--',alpha=0.5)

df1=rbdy2_df.groupby(['j','i'])['nb_connect'].apply(np.mean).unstack()
df = pd.DataFrame(np.nan,index= np.arange(N+1),columns=np.arange(N+1)).drop([0]).drop([0], axis=1)
df.loc[df1.index,df1.columns]=df1
sns.heatmap(df,vmin=-0.5,vmax=9.5,cmap=sns.color_palette("coolwarm", 10,as_cmap=False),
            ax=ax1,cbar_kws={'ticks': np.arange(10),"shrink": .8})
ax1.invert_yaxis()
ax1.axis('equal')
    

df2=rbdy2_df.groupby(['j','i'])['nb_connect'].apply(np.mean).unstack()
df = pd.DataFrame(np.nan,index= np.arange(N+1),columns=np.arange(N+1)).drop([0]).drop([0], axis=1)
df.loc[df2.index,df2.columns]=df2
sns.heatmap(df,vmin=-0.5,vmax=6.5,cmap=sns.color_palette("coolwarm", 7,as_cmap=False),
            ax=ax2,cbar_kws={'ticks': np.arange(7),"shrink": .8})
ax2.invert_yaxis()
ax2.axis('equal')

bande_k['h (unit:d)']=bande_k.apply(lambda x: x.name*lgrid/R_cir/2,axis=1, result_type='expand')
sns.lineplot(data=bande_k.set_index('h (unit:d)'),ax=ax3)

plt.title('the end')
plt.show()

# =============================================================================

bande_df['h (unit:d)'] = bande_df.apply(lambda x: x.name*lgrid/R_cir/2,axis=1, result_type='expand')

fig= plt.figure(figsize=(9, 16))
ax1 = fig.add_subplot(411)
ax2 = fig.add_subplot(412)
ax3 = fig.add_subplot(413)
ax4 = fig.add_subplot(414)

bande_df.plot('h (unit:d)', 'volume_fraction', yerr='volume_fraction_std', ax=ax1,legend = False)
ax1.set_xlabel('')
ax1.set_ylabel('fraction volume($\phi$)')

bande_df.plot('h (unit:d)', 'v_ex', yerr='v_ex_std', ax=ax2,legend = False)
ax2.plot([0,np.max(bande_df['h (unit:d)'])],[0,0],'r--', alpha=0.3)
ax2.set_xlabel('')
ax2.set_ylabel('velocity ($V_x/\sqrt{gd}$)')

bande_df.plot('h (unit:d)', 'Temperature_x', yerr='Temperature_x_std', ax=ax3,legend = False)
ax3.set_xlabel('')
ax3.set_ylabel('Temperature ($T_x$)')

bande_df.plot('h (unit:d)', 'nb_connect', yerr='nb_connect_std', ax=ax4)
bande_df.plot('h (unit:d)', 'nb_voisin', yerr='nb_voisin_std', ax=ax4)
ax4.set_ylabel('coordinate number')

width_band='('+str(int(demi_bande/R_cir))+'d)'
ax1.set_title('Time average in the band'+width_band)
plt.savefig('bande1'+width_band+'.pdf', dpi=200)
plt.show()

# =============================================================================

fig= plt.figure(figsize=(9, 16))
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)

for Fxx in ['F11','F12','F21','F22']:
    Fxx_std=str(Fxx)+'_std'
    bande_df.plot('h (unit:d)', Fxx, yerr=Fxx_std, ax=ax1)
ax1.set_xlabel('')
ax1.set_ylabel('Fabric tensor')

bande_df.plot('h (unit:d)', 'F_i', yerr='F_i_std', ax=ax2)
bande_df.plot('h (unit:d)', 'F_ii', yerr='F_ii_std', ax=ax2)
ax2.set_xlabel('')
ax2.set_ylabel('eigivalues')

ax1.set_title('Time average in the band'+width_band)
plt.savefig('bande2'+width_band+'.pdf', dpi=200)
plt.show()





