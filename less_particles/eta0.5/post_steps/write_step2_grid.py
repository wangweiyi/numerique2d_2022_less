import tqdm
import numpy as np
import pandas as pd
from scipy import stats

import matplotlib.pyplot as plt
import seaborn as sns

import sys,os

path_my_post=os.path.join('..','..','..','src') 
sys.path.append(path_my_post)
import mypost_voronoi as mpv

import fnmatch
nb_files_step1= len(fnmatch.filter(os.listdir('./pickles'),'post_step1.pkl*'))

import pickle
# read parametres when generate the sample
f=open('../sample.p','rb')
R_cir=pickle.load(f)
n_branch=pickle.load(f)
r_p=pickle.load(f)
rhog_num=pickle.load(f)
Rext=pickle.load(f)
Omegaz=pickle.load(f)
f.close()


# =============================================================================
lgrid=3*R_cir           #length of grid
N=2*int(Rext/lgrid)     #size of grid N*N
bins=np.arange(-Rext,Rext,lgrid)

#bande sigma
demi_bande=3*R_cir
n=demi_bande/lgrid
i_bande=np.arange(0.5*N+1-n,0.5*N+1+n)    # the grid i in the bande

# =============================================================================

def readPklStep1(k):
    name='post_step1.pkl'+str(k)
    path='./pickles/'+name
    f=open(path,'rb')
    post_step1_df=pickle.load(f)
    f.close()
    
    return post_step1_df



void_df=pd.DataFrame()
needs = {'volume_fraction':void_df,'v_x':void_df,'v_y':void_df,
          'nb_voisin':void_df,'nb_connect':void_df,
          'F11':void_df, 'F12':void_df, 'F21':void_df,'F22':void_df,
          'F_i':void_df,'F_ii':void_df}


agg_methode={'volume_fraction':np.mean,'v_x':np.mean,'v_y':np.mean,
             'nb_voisin':np.mean,'nb_connect':np.mean,
             'F11':np.mean, 'F12':np.mean, 'F21':np.mean,'F22':np.mean,
             'F_i':np.mean,'F_ii':np.mean}

surface_df=void_df
linereg_df=void_df
df=void_df

start=80
for k in tqdm.trange(start, 20+nb_files_step1):
# for k in tqdm.trange(start, 200):
    post_step1_df=readPklStep1(k)
    post_step1_dict=post_step1_df['post_step1_dict']
    rbdy2_df=post_step1_df['rbdy2_df']
# =============================================================================

    linereg = post_step1_df['post_step1_dict']['linereg']
    linereg_df=pd.concat([linereg_df,pd.DataFrame(linereg)],axis=1)
    
    cd_array=np.vstack(rbdy2_df.cd)
    layer_df=rbdy2_df[['i','j']].copy()
    layer_df[str(k)]=cd_array[:,1]+R_cir+Rext
    surface_df=pd.concat([surface_df,layer_df.groupby('i')[str(k)].max()],axis=1)
    
# =============================================================================
    v_xy=np.vstack(rbdy2_df.v_xy)
    rbdy2_df['v_x']=v_xy[:,0]
    rbdy2_df['v_y']=v_xy[:,1]
    
    grid_k=rbdy2_df.groupby('igrid').agg(agg_methode)
    
    for key, need_df in needs.items():
        needs[key] = pd.concat([need_df,grid_k.loc[:,key]],axis=1)
        
    
grid_dict = {}
for key, need_df in needs.items():
    grid_dict.update({key:need_df.mean(axis=1)})

grid_df=pd.DataFrame(grid_dict)
print('\n',grid_df)

layer_need = pd.DataFrame({'free_surface':surface_df.mean(axis=1),
                           'free_surface_std':surface_df.std(axis=1)})

name='./step2_grid_xy.p'
f = open(name, "wb" )
pickle.dump(grid_df, f )
pickle.dump(layer_need, f )
f.close()

# =============================================================================

fig= plt.figure(figsize=(16, 9))

theta = np.arctan(post_step1_dict['linereg'].slope)
s=np.sin(theta);c=np.cos(theta)

theta_t = np.linspace(0, 2 * np.pi, 200)
xt=np.cos(theta_t)
yt=np.sin(theta_t)

xs=0.5*N*np.array([-s,s])+0.5*N
ys=0.5*N*np.array([c,-c])+0.5*N

ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)

ratio=Rext/R_cir/2
ax1.plot(ratio*(xt+1),ratio*(yt+1),label='tambour')
ax2.plot(ratio*(xt+1),ratio*(yt+1),label='tambour')

layer_need=layer_need.applymap(lambda x: x/R_cir/2)
layer_need['x (unit:d)'] = layer_need.apply(lambda x: (x.name-0.5)*lgrid/R_cir/2,axis=1, result_type='expand')

layer_need.plot('x (unit:d)', 'free_surface', yerr='free_surface_std', ax=ax2, label= 'free surface')

df=grid_df[['v_x','v_y']].reset_index()
df['i'] = (df['index'].mod(N)-0.5)*lgrid/R_cir/2
df['j'] = (df['index'].floordiv(N)-0.5)*lgrid/R_cir/2
stream = df[['j','i','v_x','v_y']].to_numpy()

ax1.quiver(stream[:,0], stream[:,1], stream[:,2], stream[:,3])
ax1.axis('equal')
ax1.set_ylabel('y (unit:d)')
ax1.set_title('velocity field')

stream_df = df[['j','i','v_x','v_y']]
u=df[['j','i','v_x']].pivot(index='i', columns='j', values='v_x').fillna(0)
v=df[['j','i','v_y']].pivot(index='i', columns='j', values='v_y').fillna(0)
X=df[['j','i','v_x']].pivot(index='i', columns='j', values='j').fillna(method='ffill').fillna(method='bfill').to_numpy()
Y=df[['j','i','v_y']].pivot(index='i', columns='j', values='i').fillna(axis=1,method='ffill').fillna(axis=1,method='bfill').to_numpy()

plt.streamplot(X, Y, u, v, density = 0.5,color='k')

ax2.axis('equal')
ax2.set_ylabel('y (unit:d)')
ax2.set_title('velocity stream')
plt.savefig('field.pdf',dpi=200)


# ======================== Aniamtion streamplot =================================

import matplotlib.animation as animation
# needs['v_x'], needs['v_y'], surface_df

surface_df=surface_df.applymap(lambda x: x/R_cir/2)
surface_df['x (unit:d)'] = surface_df.apply(lambda x: (x.name-0.5)*lgrid/R_cir/2,axis=1, result_type='expand')


fig= plt.figure(figsize=(16, 9))
ax1=plt.subplot(121)
ax2=plt.subplot(122)

ratio=Rext/R_cir/2
ax1.plot(ratio*(xt+1),ratio*(yt+1))
ax2.plot(ratio*(xt+1),ratio*(yt+1))
ax1.axis('equal')
ax2.axis('equal')

x=surface_df['x (unit:d)'].to_numpy()


line1, = ax1.plot([], [], label= 'free surface') 
line2, = ax2.plot([], [], label= 'free surface') 
line3, = ax2.plot([], [], label= 'line regresse') 
ax1.legend()
ax2.legend()
def animate(iter):
    ax1.collections = [] # clear lines streamplot
    ax1.patches = [] # clear arrowheads streamplot
    
    y1=surface_df.iloc[:,iter].to_numpy()
    line1.set_data(x, y1)
    line2.set_data(x, y1)
    
    slope=linereg_df.iloc[0,iter];intercept=(linereg_df.iloc[1,iter]+(1-slope)*Rext)/R_cir/2
    line3.set_data(x, slope*x+intercept)
    
    df=pd.concat([needs['v_x'].iloc[:,iter],needs['v_y'].iloc[:,iter]],axis=1).reset_index()
    df['i'] = (df['index'].mod(N)-0.5)*lgrid/R_cir/2
    df['j'] = (df['index'].floordiv(N)-0.5)*lgrid/R_cir/2
    
    u=df[['j','i','v_x']].pivot(index='i', columns='j', values='v_x').fillna(0)
    v=df[['j','i','v_y']].pivot(index='i', columns='j', values='v_y').fillna(0)
    X=df[['j','i','v_x']].pivot(index='i', columns='j', values='j').fillna(method='ffill').fillna(method='bfill').to_numpy()
    Y=df[['j','i','v_y']].pivot(index='i', columns='j', values='i').fillna(axis=1,method='ffill').fillna(axis=1,method='bfill').to_numpy()
    
    ax1.streamplot(X, Y, u, v, density = 0.5,color='k')
    return line1,line2,line3,stream

anim =  animation.FuncAnimation(fig, animate, frames=linereg_df.shape[1], interval=500, blit=False, repeat=False)
anim.save('./stream_'+str(linereg_df.shape[1])+'.gif', writer='Pillow', fps=5)

# =============================================================================





