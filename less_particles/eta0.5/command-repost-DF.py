import numpy as np
import pandas as pd
import tqdm
from pylmgc90.chipy import *
import sys,os


path_my_post=os.path.join('..','..','src') 
sys.path.append(path_my_post)
import mypost_read_lmgc90_remakeDf as mypost


os.makedirs('pickles_remake_Df', exist_ok=True)  

import fnmatch
nb_files = len(fnmatch.filter(os.listdir('./OUTBOX'),'DOF.OUT*'))
nb_files_remake= len(fnmatch.filter(os.listdir('./pickles_remake_Df'),'*.pkl'))
print(nb_files_remake,'/',nb_files-20)


import pickle
f=open('./sample.p','rb')
R_cir=pickle.load(f)
n_branch=pickle.load(f)
r_p=pickle.load(f)
rhog_num=pickle.load(f)
Rext=pickle.load(f)
Omegaz=pickle.load(f)
f.close()

checkDirectories()

Initialize()

# desactivation des messages de log
utilities_DisableLogMes()

####
# info gestion du temps
dt = 1.e-3
theta = 0.5

# bavardage de certaines fonctions
echo = 0

# info generation fichier visu
freq_display = 100
freq_write   = 100

ref_radius   = r_p

# info contact

#       123456789012345678901234567890
type = 'Stored_Delassus_Loops         '
norm = 'Quad '
tol = 0.1666e-3
relax = 1.0
gs_it1 = 51
gs_it2 = 1001

restart_outbox = 20

SetDimension(2)
### definition des parametres du calcul ### 
#utilities_logMes('INIT TIME STEPPING')
#TimeEvolution_SetTimeStep(dt)
#Integrator_InitTheta(theta)

### lecture du modele ###

### model reading ###
utilities_logMes('READ BODIES')
RBDY2_ReadBodies()

utilities_logMes('READ INI DOF')
ReadIniDof(restart_outbox)

utilities_logMes('READ BEHAVIOURS')
ReadBehaviours()
LoadBehaviours()

#LOADS
LoadTactors()

utilities_logMes('READ INI Vloc Rloc')
ReadIniVlocRloc(restart_outbox)

utilities_logMes('READ DRIVEN DOF')
RBDY2_ReadDrivenDof()

### post2D ##
#OpenDisplayFiles(restart=20)

utilities_logMes('COMPUTE MASS')
RBDY2_ComputeMass()

rbdy2_exclue = mypost.readNRbdy2Invisible()

start = restart_outbox+nb_files_remake
#start = 20

for k in tqdm.trange(start, nb_files+1):
    evol_connect_df = pd.DataFrame(columns = ['Time'])
    #
    utilities_logMes('READ OUT DOF')
    ReadIniDof(k)
    #
    utilities_logMes('READ OUT Vloc Rloc')
    ReadIniVlocRloc(k)
    #
    rbdy2_df,DISKx_df,POLYG_df=mypost.createDfRBDY2(rbdy2_exclue=rbdy2_exclue)
    inters_df=mypost.createDfInters(rbdy2_df,DISKx_df,rbdy2_exclue)
    
    evol_dict={'Time': TimeEvolution_GetTime(),
                 'rbdy2_df': rbdy2_df,
                 'DISKx_df': DISKx_df,
                 'POLYG_df': POLYG_df,
                 'inters_df':inters_df}

    name='post_pickle_'+str(k)
    path='./pickles_remake_Df/'+name+'.pkl'
    f = open(path, "wb" )
    pickle.dump(evol_dict, f )
    f.close()
    print('writed:'+ str(k))
   
   #
   ### post2D ###
   #WriteDisplayFiles(freq_display,ref_radius)

   ### wrtieout handling ###
   # overall_CleanWriteOutFlags()

#CloseDisplayFiles()

Finalize()


