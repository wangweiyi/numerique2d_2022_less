import numpy as np
import math

from pylmgc90.chipy import *
from scipy import integrate
import pandas as pd
import matplotlib.pyplot as plt
import copy

import pickle
f=open('./sample.p','rb')
R_cir=pickle.load(f)
n_branch=pickle.load(f)
r_p=pickle.load(f)
rhog_num=pickle.load(f)
Rext=pickle.load(f)
Omegaz=pickle.load(f)
f.close()

g=9.81


def keepHalfDrum():
    rbdy2_exclue = []
    nbr = RBDY2_GetNbRBDY2()
    for ir in range(2,nbr+1):
        cd = RBDY2_GetBodyVector('Coor_', ir)
        if cd[1]> Rext:
            RBDY2_SetInvisible(ir)
            rbdy2_exclue.append(ir)
    
    print('nb_particle: ', RBDY2_GetNbRBDY2()-1-len(rbdy2_exclue))
    f = open("rbdy2_exclue.p", "wb" )
    pickle.dump(rbdy2_exclue, f )
    f.close()

def readNRbdy2Invisible():
    import fnmatch
    pickle_files = fnmatch.filter(os.listdir('.'),'*.p')
    name = 'rbdy2_exclue.p'
    if name in pickle_files:
        f=open('./rbdy2_exclue.p','rb')
        rbdy2_exclue=pickle.load(f)
        f.close()
    else:
        rbdy2_exclue = []
        
    return rbdy2_exclue


def createDfRBDY2(rbdy2_exclue):
    #Vide DataFrame for the stockage of RBDY2
    rbdy2_df = pd.DataFrame(columns = ['number'])
    #Vide DataFrame for the stockage of all of the DISKx
    DISKx_df = pd.DataFrame()
    #set function for DISKx
    diskx2bdyty = DISKx_GetDISKx2BDYTY()
    nbd = DISKx_GetNbDISKx()
    
    #Vide DataFrame for the stockage of all of the DISKx
    POLYG_df = pd.DataFrame()
    #set function for POLYGx
    polygx2bdyty = POLYG_GetPOLYG2BDYTY()
    nbp =POLYG_GetNbPOLYG()
    #Note global number of DISKx for each RBDY2
    ids_dict={}
    for idisk in range(1,nbd+1):
        ibdty= diskx2bdyty[idisk-1]
        ir = int(ibdty[0])
        if ir in rbdy2_exclue or ir==1:
            continue
        elif ir in ids_dict.keys():
            ids_dict[ir].append(idisk)
            continue
        else:
            ids_dict.update({ir:[idisk]})
            
    #Note global number of POLYG for each RBDY2
    withPOLYG=False
    if nbp==0:
        print('no POLYG')
    elif nbp!=0:
        withPOLYG=True
        ips_dict={}
        for ipolyg in range(1,nbp+1):
            ibdty= polygx2bdyty[ipolyg-1]
            ir = int(ibdty[0])
            if ir in rbdy2_exclue or ir==1:
                continue
            elif ir in ips_dict.keys():
                ips_dict[ir].append(ipolyg)
                continue
            else:
                ips_dict.update({ir:[ipolyg]})

    #Iterate the RBDY2
    for key, ids in ids_dict.items():
        cd = RBDY2_GetBodyVector('Coor_', key)
        cd = np.array([float(cd[0]-Rext),float(cd[1]-Rext)])
        v = RBDY2_GetBodyVector('V____', key)
        w = round(v[2],6)
        v = [round(v[0],6),round(v[1],6)]

        #Iterate through the DISK in each RBDY2
        for idisk in ids:
            ibdty= diskx2bdyty[idisk-1]
            cd_disk = DISKx_GetContactorCoor(idisk)
            cd_disk = np.array([float(cd_disk[0]-Rext),float(cd_disk[1]-Rext)])
            rr=DISKx_GetContactorRadius(idisk)
            DISK_dict={'number': key,
                        'id_in_number':ibdty[1],
                        'iDISKx': idisk,
                        'cd_disk': cd_disk[0:2],
                        'position_x':cd_disk[0],
                        'position_y':cd_disk[1],
                        'r_p':rr}
            DISKx_df=DISKx_df.append(DISK_dict, ignore_index=True)
        
        #Iterate through the POLYG in each RBDY2
        ips=[]
        if withPOLYG==True:
            ips=ips_dict[key]
            for ipolyg in ips:
                ibdty= polygx2bdyty[ipolyg-1]
                vertex_polyg = POLYG_GetVertex(ipolyg,8).reshape(4,2)
    
                cd_polyg=np.mean(np.array(vertex_polyg), axis=0)
                POLYG_dict={'number': key,
                            'iPOLYG': ipolyg,
                            'id_in_number':ibdty[1],
                            'vertex_polyg':vertex_polyg,
                            'cd_polyg':cd_polyg-Rext}
                POLYG_df=POLYG_df.append(POLYG_dict, ignore_index=True)
            
        #Stockage of RBDY2
        rbdy2_df = rbdy2_df.append({'number': key,
                                    'iDISKx': ids,
                                    'iPOLYG': ips,
                                    'r_p':rr,
                                    'cd': cd,
                                    'v_xy':v,
                                    'w':w}, ignore_index=True)

        
    
    rbdy2_df=rbdy2_df.astype({'number': 'int32'})
    DISKx_df=DISKx_df.astype({'number': 'int32','id_in_number': 'int32'})
    if withPOLYG==True:
        POLYG_df=POLYG_df.astype({'number': 'int32','id_in_number': 'int32'})
        
    return rbdy2_df,DISKx_df,POLYG_df


test_mode=False
def createDfInters(rbdy2_df,DISKx_df,rbdy2_exclue):
    nbr = RBDY2_GetNbRBDY2()-1
    inters_df = pd.DataFrame(columns = ['particle1','particle2']) 
    
    IDs = [DKDKx_ID,DKPLx_ID]
    for ID in IDs:
        if inter_handler_2D_getNb(ID): 
            # coor[0:1], n[0:1], rn, rt, gap
            inters = inter_handler_2D_getAll(ID)
            # icdbdy, icdtac, cdmodel, ianbdy, iantac, anmodel, status
            idatas = inter_handler_2D_getAllIdata(ID)
    
            for idx,inter in enumerate(inters):
                if int(idatas[idx,0]) in rbdy2_exclue or int(idatas[idx,3]) in rbdy2_exclue:
                    continue
                if inter[4] != 0:
                    n=inter[2:4]
                    # repere t,n
                    t=np.array((n[1],-n[0]))
                    inters_df = inters_df.append({'particle1': int(idatas[idx,0]),
                                                  'icdtac':int(idatas[idx,1]),
                                                  'particle2': int(idatas[idx,3]),
                                                  'iantac':int(idatas[idx,4]),
                                                  'nb_connect': 1,
                                                  'cd_inter': inter[0:2]-Rext,
                                                  'rn':inter[4],
                                                  'rt':inter[5],
                                                  'Rx': inter[4]*n[0]+inter[5]*t[0],
                                                  'Ry': inter[4]*n[1]+inter[5]*t[1],
                                                  'type_connect_element': ID}, ignore_index=True)

    def eline_width(x):
        if x<1 :
            return 3
        else:
            return 3*abs(row['rn'])
    
    # test figure
    if test_mode==True:
        fig = plt.figure(figsize=(16, 9),dpi=100)
        ax = fig.add_subplot(111)
        theta_t = np.linspace(0, 2 * np.pi, 200)
        xt=Rext*(np.cos(theta_t))
        yt=Rext*(np.sin(theta_t))
        plt.plot(xt,yt,label='tambour')
        
        DISKx_df=DISKx_df.set_index(['number','id_in_number'])
        print(DISKx_df.head())
        draw_polyg=False
        if (R_cir-r_p)/R_cir>0.5:
            draw_polyg=True
        for index, row in rbdy2_df.copy().iterrows():
            for indexd, rowd in DISKx_df.loc[row['number']].copy().iterrows():
                cdx=rowd['position_x'];cdy=rowd['position_y']
                disk=plt.Circle((cdx,cdy),row['r_p'],alpha=1)
                ax.add_patch(disk)
                if draw_polyg==True:
                    vec=[row['cd'][0]-cdx,row['cd'][1]-cdy]
                    l=np.linalg.norm(vec)
                    vec_norm=vec/l
                    vec_p=[vec_norm[1],-vec_norm[0]]
                    xy=[row['cd'][0]+vec_p[0]*row['r_p'],row['cd'][1]+vec_p[1]*row['r_p']]
                    if vec_norm[1]>0:
                        rectangle=plt.Rectangle(xy,l,2*row['r_p'],angle=np.arccos(vec_norm[0])*180/np.pi)
                    else:
                        rectangle=plt.Rectangle(xy,l,2*row['r_p'],angle=360-np.arccos(vec_norm[0])*180/np.pi)
                    ax.add_patch(rectangle)
# =============================================================================
##chaine de force particle 2 particle
#         for index, row in connect_df1.iterrows():
#             cd1 = RBDY2_GetBodyVector('Coor_', int(index[0]))
#             cd2 = RBDY2_GetBodyVector('Coor_', int(index[1]))
#             vector=cd2[0:2]-cd1[0:2]
#             norm=np.linalg.norm(vector)
#             vector_normalized=vector/norm
#             R_pp=np.dot([row['Rx'],row['Ry']],vector_normalized)
#             plt.plot([cd1[0],cd2[0]],[cd1[1],cd2[1]],color='red', linewidth=abs(R_pp))
# =============================================================================
#chaine de force disk 2 disk
        rbdy2_df=rbdy2_df.set_index('number')
        for index, row in inters_df.iterrows():
            if row['type_connect_element']==1.0:
                cd1=rbdy2_df.loc[row['particle1'],'cd']
                cd2=rbdy2_df.loc[row['particle2'],'cd']
#                disk1=rbdy2_df.loc[row['particle1'],'data_DISKx'].iloc[int(row['icdtac'])-1]
#                disk2=rbdy2_df.loc[row['particle2'],'data_DISKx'].iloc[int(row['iantac'])-1]
#                cd1 = [disk1['position_x'],disk1['position_y']]
#                cd2 = [disk2['position_x'],disk2['position_y']]
                plt.plot([cd1[0],row['cd_inter'][0],cd2[0]],[cd1[1],row['cd_inter'][1],cd2[1]],color='black', linewidth=eline_width(row['rn']))
            elif row['type_connect_element']==3.0:
                cd1=rbdy2_df.loc[row['particle1'],'cd']
                cd2=rbdy2_df.loc[row['particle2'],'cd']
                plt.plot([cd1[0],row['cd_inter'][0],cd2[0]],[cd1[1],row['cd_inter'][1],cd2[1]],ls='-',color='red', linewidth=eline_width(row['rn']))
            else:
                print(row['type_connect_element'])
# =============================================================================
#chaine de force disk 2 disk

    
#        rbdy2_df=rbdy2_df.set_index('number')

#        for index, row in inters_df.iterrows():
#            if row['type_connect_element']==1.0:
#                disk1=rbdy2_df.loc[row['particle1'],'data_DISKx'].iloc[int(row['icdtac'])-1]
#                disk2=rbdy2_df.loc[row['particle2'],'data_DISKx'].iloc[int(row['iantac'])-1]
#                cd1 = [disk1['position_x'],disk1['position_y']]
#                cd2 = [disk2['position_x'],disk2['position_y']]
#                plt.plot([cd1[0],row['cd_inter'][0],cd2[0]],[cd1[1],row['cd_inter'][1],cd2[1]],color='black', linewidth=eline_width(row['rn']))
#            elif row['type_connect_element']==3.0:
#                disk1=rbdy2_df.loc[row['particle1'],'data_DISKx'].iloc[int(row['icdtac'])-1]
#                polyg2=rbdy2_df.loc[row['particle2'],'data_POLYGx'].set_index('id_in_number').loc[int(row['iantac'])]
##                print(polyg2.loc[int(row['iantac'])])
#                cd1 = [disk1['position_x'],disk1['position_y']]
#                cd2 = [polyg2['cd_polyg'][0],polyg2['cd_polyg'][1]]
#                plt.plot([cd1[0],row['cd_inter'][0],cd2[0]],[cd1[1],row['cd_inter'][1],cd2[1]],ls='-',color='red', linewidth=eline_width(row['rn']))
#            else:
#                print(row['type_connect_element'])
                
                
        time=TimeEvolution_GetTime()
        plt.title('Time= '+str(time)+'s')
        plt.axis('equal')
        plt.show()
        
    return inters_df














