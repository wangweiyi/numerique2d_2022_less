import numpy as np
import math

from scipy import integrate
import pandas as pd
import copy
import time

import pickle
f=open('../sample.p','rb')
R_cir=pickle.load(f)
n_branch=pickle.load(f)
r_p=pickle.load(f)
rhog_num=pickle.load(f)
Rext=pickle.load(f)
Omegaz=pickle.load(f)
f.close()

g=9.81

import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull,Voronoi
from shapely.geometry import MultiPoint, Point, Polygon

clist=['salmon','gold','limegreen','lightblue','cornflowerblue','lightgray','pink','violet','palegreen']
    
def halfDrumLeft(y):
    theta=np.arcsin(y/Rext)
    x=-np.cos(theta)*Rext
    return x

def halfDrumRight(y):
    theta=np.arcsin(y/Rext)
    x=np.cos(theta)*Rext
    return x

def VoronoiDiagram(rbdy2_df,DISKx_df,mask_points,draw=False):
    # start_time = time.time()

    theta_t = np.linspace(0, 2 * np.pi, 200)
    xt=Rext*(np.cos(theta_t))
    yt=Rext*(np.sin(theta_t))
    points_drum=np.stack((xt,yt), axis=1)
    
    points_df=pd.DataFrame()
    n_p=int(R_cir/r_p/2)
    for k in range(0, len(rbdy2_df)):
        row=rbdy2_df.iloc[k]
        for indexd,rowd in DISKx_df.loc[DISKx_df['number']==row.name].iterrows():
            cd_disk_x=rowd['position_x'];cd_disk_y=rowd['position_y'];rr=rowd['r_p']
            xs=np.linspace(row['cd'][0],cd_disk_x,n_p+1)
            ys=np.linspace(row['cd'][1],cd_disk_y,n_p+1)
            xys= {'cd': list(zip(xs,ys))}
            df = pd.DataFrame(xys, columns=['cd'])
            df['number']=row.name
            df['iDISKx']=rowd['iDISKx']
            df['r_p']=rr
            points_df=pd.concat([points_df,df])
        
    # points=DISKx_df['cd_disk'].tolist()
    points=points_df['cd'].tolist()
    vor=Voronoi(points)
    regions, vertices = voronoi_finite_polygons_2d(vor)
    
    pts0 = MultiPoint([Point(i) for i in mask_points])
    mask0 = pts0.convex_hull
    pts1 = MultiPoint([Point(i) for i in points_drum])
    mask1 = pts1.convex_hull
    # mask = mask1
    pts2 = MultiPoint([Point(i) for i in points])
    mask2 =pts2.convex_hull.buffer(0.5*R_cir)
    
    mask = mask0
    mask = mask.union(mask2)
    mask = mask.intersection(mask1)
    
    new_vertices = []
    if draw=='Debug':
        try:
            for region in regions:
                polygon = vertices[region]
                shape = list(polygon.shape)
                shape[0] += 1
                p = Polygon(np.append(polygon, polygon[0]).reshape(*shape)).intersection(mask)
                poly = (np.array(p.exterior.coords)).tolist()
                new_vertices.append(poly)
        except:
            fig, ax = plt.subplots(figsize=(12, 9))
            plt.plot(xt,yt,label='tambour')
            for region in regions:
                polygon = vertices[region]
                shape = list(polygon.shape)
                shape[0] += 1
                p = Polygon(np.append(polygon, polygon[0]).reshape(*shape)).intersection(mask)
                poly = (np.array(p.exterior.coords)).tolist()
                new_vertices.append(poly)
                plt.fill(*zip(*poly), alpha=0.5,edgecolor='b')
            for indexd,rowd in DISKx_df.iterrows():
                cd_disk_x=rowd['position_x'];cd_disk_y=rowd['position_y'];rr=rowd['r_p']
                ax.add_patch(plt.Circle((cd_disk_x,cd_disk_y),rr,alpha=0.5))
            plt.axis('equal')
            plt.show()


    elif draw==True:
        fig, ax = plt.subplots(figsize=(12, 9))
        plt.plot(xt,yt,label='tambour')
        i=0
        for idx,region in enumerate(regions):
            polygon = vertices[region]
            shape = list(polygon.shape)
            shape[0] += 1
            p = Polygon(np.append(polygon, polygon[0]).reshape(*shape)).intersection(mask)
            poly = (np.array(p.exterior.coords)).tolist()
            new_vertices.append(poly)
            if np.remainder(idx,4*(n_p+1))==0:
                if i==7:
                    plt.fill(*zip(*poly), alpha=0.5, facecolor=clist[i], edgecolor='orangered')
                    i=0
                    
                else:
                    plt.fill(*zip(*poly), alpha=0.5, facecolor=clist[i], edgecolor='orangered')
                    i+=1
                    
            else:
                plt.fill(*zip(*poly), alpha=0.5, facecolor=clist[i], edgecolor='orangered')
                
            
        for k in range(0, len(rbdy2_df)):
            row=rbdy2_df.iloc[k]
            for indexd,rowd in DISKx_df.loc[DISKx_df['number']==row.name].iterrows():
                cd_disk_x=rowd['position_x'];cd_disk_y=rowd['position_y'];rr=rowd['r_p']
                ax.add_patch(plt.Circle((cd_disk_x,cd_disk_y),rr,alpha=1))
                
                vec=[row['cd'][0]-cd_disk_x,row['cd'][1]-cd_disk_y]
                l=np.linalg.norm(vec)
                vec_norm=vec/l
                vec_p=[vec_norm[1],-vec_norm[0]]
                xy=[row['cd'][0]+vec_p[0]*row['r_p'],row['cd'][1]+vec_p[1]*row['r_p']]
                if vec_norm[1]>0:
                    ax.add_patch(plt.Rectangle(xy,l,2*row['r_p'],alpha=0.5,angle=np.arccos(vec_norm[0])*180/np.pi))
                else:
                    ax.add_patch(plt.Rectangle(xy,l,2*row['r_p'],alpha=0.5,angle=360-np.arccos(vec_norm[0])*180/np.pi))
        plt.axis('equal')
        plt.show()
    else:
        for region in regions:
            polygon = vertices[region]
            shape = list(polygon.shape)
            shape[0] += 1
            p = Polygon(np.append(polygon, polygon[0]).reshape(*shape)).intersection(mask)
            poly = (np.array(p.exterior.coords)).tolist()
            new_vertices.append(poly)


    voronoi_df=points_df[['number','r_p']].copy()
    
    voronoi_df['volume_voronoi'] = voronoi_df.apply(lambda x: ConvexHull(new_vertices[x.name]).volume,axis=1)
    voronoi_df = voronoi_df.groupby('number').agg({'r_p':'mean','volume_voronoi':'sum'})
    voronoi_df['volume_fraction'] = voronoi_df.apply(lambda x: VolumeParticle(x['r_p'])/x['volume_voronoi'],axis=1)
    # print('\n',voronoi_df[['volume_voronoi','volume_fraction']])
    
    # print("---Voronoi %s seconds ---" % (time.time() - start_time))
    return voronoi_df[['volume_voronoi','volume_fraction']]

def VolumeParticle(rr):
    e=r_p/R_cir
    R = rr/e
    r=rr
    n=4
    if R-r <= r/math.tan(math.pi/n) and r < R:
        d = 2*(R-r)*math.sin(math.pi/n)
        theta1 = math.pi/2-math.pi/n
        theta2 = math.acos(d/2/r)
        theta3 = math.pi-theta2-theta1
        l = r*math.sin(theta3)
        S_particle = n*l**2/math.tan(math.pi/n)+r**2*(theta3-math.sin(2*theta3)/2)*n
        
    elif R-r > r/math.tan(math.pi/n) and r > 0:
        l_branch = R - r*(1 + 1/math.tan(math.pi/n))
        S_particle = 0.5*n*math.pi*r**2 + n*r**2/math.tan(math.pi/n) + n*l_branch*(2*r)
        
    elif R == r:
        S_particle = math.pi*R**2
        
    return S_particle

def voronoi_finite_polygons_2d(vor, radius=None):
    """
    Reconstruct infinite voronoi regions in a 2D diagram to finite
    regions.

    Parameters
    ----------
    vor : Voronoi
        Input diagram
    radius : float, optional
        Distance to 'points at infinity'.

    Returns
    -------
    regions : list of tuples
        Indices of vertices in each revised Voronoi regions.
    vertices : list of tuples
        Coordinates for revised Voronoi vertices. Same as coordinates
        of input vertices, with 'points at infinity' appended to the
        end.

    """
    if vor.points.shape[1] != 2:
        raise ValueError("Requires 2D input")

    new_regions = []
    new_vertices = vor.vertices.tolist()

    center = vor.points.mean(axis=0)
    if radius is None:
        radius = vor.points.ptp().max()

    # Construct a map containing all ridges for a given point
    all_ridges = {}
    for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices):
        all_ridges.setdefault(p1, []).append((p2, v1, v2))
        all_ridges.setdefault(p2, []).append((p1, v1, v2))
        
    # Reconstruct infinite regions
    for p1, region in enumerate(vor.point_region):
        vertices = vor.regions[region]
        if all(v >= 0 for v in vertices):
            # finite region
            new_regions.append(vertices)
            continue

        # reconstruct a non-finite region
        ridges = all_ridges[p1]
        new_region = [v for v in vertices if v >= 0]

        for p2, v1, v2 in ridges:
            if v2 < 0:
                v1, v2 = v2, v1
            if v1 >= 0:
                # finite ridge: already in the region
                continue
            # Compute the missing endpoint of an infinite ridge

            t = vor.points[p2] - vor.points[p1] # tangent
            t /= np.linalg.norm(t)
            n = np.array([-t[1], t[0]])  # normal

            midpoint = vor.points[[p1, p2]].mean(axis=0)
            direction = np.sign(np.dot(midpoint - center, n)) * n
            far_point = vor.vertices[v2] + direction * radius

            new_region.append(len(new_vertices))
            new_vertices.append(far_point.tolist())

        # sort region counterclockwise
        vs = np.asarray([new_vertices[v] for v in new_region])
        c = vs.mean(axis=0)
        angles = np.arctan2(vs[:,1] - c[1], vs[:,0] - c[0])
        new_region = np.array(new_region)[np.argsort(angles)]

        # finish
        new_regions.append(new_region.tolist())
        
    return new_regions, np.asarray(new_vertices)


